use abstract_factory::train::*;
use abstract_factory::train_factory::*;
use super::TrainBuilder;

pub struct MagLevBuilder {
    _factory: Box<AbstractTrainFactory>,
    _train: Train,
    _cargo_flag: bool,
    _people_flag: bool,
}

impl MagLevBuilder {
    pub fn new(axis: bool) -> MagLevBuilder{
        let train = Train::new();
        if axis{
            MagLevBuilder {_factory: box AxisTrainFactory, _train: train, _cargo_flag: false, _people_flag: false}
        }
        else{
            MagLevBuilder {_factory: box NonAxisTrainFactory, _train: train, _cargo_flag: false, _people_flag: false}    
        }
        
    }
}

impl TrainBuilder for MagLevBuilder {
    fn build_locomotive(&mut self, len:u32, lt:LocomotiveType){
        if len > 5{
            let loc = match lt {
                LocomotiveType::Electric => self._factory.create_electric_locomotive(len),
                LocomotiveType::Diesel => self._factory.create_diesel_locomotive(len),
            };
            self._train._locomotives.push(loc);
        } else {
            println!("locomotive is too short");
        }
    }
    fn build_car(&mut self, len:u32, ct:CarType){
        match ct {
                CarType::Cargo => {
                    if self._people_flag {
                        self._cargo_flag = true;
                    }
                    let car = self._factory.create_cargo_car(len);
                    self._train._cars.push(car);
                },
                CarType::Passenger => {
                    if self._cargo_flag {
                        println!("cannot add more passanger cars");
                    } else {
                        self._people_flag = true;
                        let car = self._factory.create_passenger_car(len);
                        self._train._cars.push(car);
                    }

                },
        };
    }
    fn get_result(&mut self) -> &Train{
        while self._train.loc_len()*20 < self._train.len() {
             self.build_locomotive(10,LocomotiveType::Diesel)
         } 
        &self._train
    }
}