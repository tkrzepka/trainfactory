use abstract_factory::train::Train;
use abstract_factory::train::LocomotiveType;
use abstract_factory::train::CarType;

pub use self::mag_lev_builder::MagLevBuilder;

pub mod mag_lev_builder;

pub trait TrainBuilder {
    fn build_locomotive(&mut self, len:u32, lt:LocomotiveType);
    fn build_car(&mut self, len:u32, ct:CarType);
    fn get_result(&mut self) -> &Train;
}