#![feature(box_syntax)] 
//Budujemy reprezentacje pociagow. Pociagi skladaja sie z wagonow, lokomotyw. Reprezentacje budujemy dla zabawy, wiec wszystkie wymagania sa co najmniej bardzo daleko idacymi uproszczeniami ale chcielibysmy, zeby taki sklad wykladal "nie calkiem nierealistycznie", dlatego dla kazdego wagonu mamy okreslona przynajmniej jego aktualna dlugosc oraz podstawowy typ (towarowy/osobowy). Dla lokomotywy mamy to samo (dlugosc i typ spalinowa/elektryczna). W podstawowej wersji tyle nam wystarczy. Mozemy miec tez rodzine obiektow bardziej szczegolowych z okreslona powiedzmy liczba osi.
//Zalozmy, ze przy tworzeniu pociagu po prostu dodajemy po kolei lokomotywy i wagony.
//Jezeli tworzone sa obiekty z liczba osi, ta liczba jest wyznaczana na podstawie dlugosci (np powyzej 16m 4 osie, ponizej 2 - dla lokomotyw wiecej) w czasie tworzenia. Wybor, ktorej wersji pociagow uzyc (z osiami, czy bez) jest dokonywany na poczatku, a pozniejszy kod nie musi od niego zalezec, mozna tu wiec uzyc fabryki abstrakcyjnej.
//Przy tworzeniu ostatecznego pociagu nalezy zapewnic, zeby laczna dlugosc lokomotyw wyniosla przynajmniej, powiedzmy 1/20 pociagu, nawet jesli orgyginalnie nie dodalismy ich tyle, przy czym lokomotywy powinny miec mniej wiecej realistyczne wymiary(nie 5m) ponadto wszystkie wagony pasazerskie powinny byc obok siebie. Do wymuszenia, aby kazdy nowutworzony obiekt spelnial te warunki, mozna np uzyc wzorca Budowniczego.
//Program powinien umozliwic ustawienie, czy pociag ma znac liczbe swoich osi, a pozniej dodawanie kolejnych wagonow i lokomotyw i wyswietlanie efektu(pociagu) inaczej w zaleznosci, czy mamy liczbe osi, czy nie. Tym razem klasy zawierajace dane moga umiec sie same wyswietlic.

extern crate wzorce2;

use wzorce2::abstract_factory::train::*;
use wzorce2::builder::*;


struct Director{
    builder: Box<TrainBuilder>,
}

impl Director{
    fn construct(&mut self){
        loop {
            println!("1. add locomotive / 2. add car / 0. finish creation");
            let mut input = String::new();
            std::io::stdin().read_line(&mut input).ok();
            let input_num: Result<u32, _> = input.trim().parse();

            let num = match input_num {
                Ok(num) => num,
                Err(_) => {
                    println!("Please input a number!");
                    continue;
                }
            };
            match num {
                1 => {
                    println!("1. electric locomotive / 2. diesel locomotive / 0. cancel");
                    input = String::new();
                    std::io::stdin().read_line(&mut input).ok();
                    let input_num: Result<u32, _> = input.trim().parse();

                    let num = match input_num {
                        Ok(num) => num,
                        Err(_) => {
                            println!("Please input a number!");
                            continue;
                        }
                    };
                        
                    match num {
                        1 => {
                            println!("provide length:");
                            input= String::new();
                            std::io::stdin().read_line(&mut input).ok();
                            let input_num: Result<u32, _> = input.trim().parse();

                            let num = match input_num {
                                Ok(num) => num,
                                Err(_) => {
                                    println!("Please input a number!");
                                    continue;
                                }
                            };
                            self.builder.build_locomotive(num,LocomotiveType::Electric);
                        },
                        2 => {
                            println!("provide length:");
                            input = String::new();
                            std::io::stdin().read_line(&mut input).ok();
                            let input_num: Result<u32, _> = input.trim().parse();

                            let num = match input_num {
                                Ok(num) => num,
                                Err(_) => {
                                    println!("Please input a number!");
                                    continue;
                                }
                            };
                            self.builder.build_locomotive(num,LocomotiveType::Diesel);
                        },
                        0 => continue,
                        _ => println!("wrong number: {}",num),
                    };
                },
                2 => {
                    println!("1. Passenger car / 2. Cargo car / 0. cancel");
                    input = String::new();
                    std::io::stdin().read_line(&mut input).ok();
                    let input_num: Result<u32, _> = input.trim().parse();

                    let num = match input_num {
                        Ok(num) => num,
                        Err(_) => {
                            println!("Please input a number!");
                            continue;
                        }
                    };
                        
                    match num {
                        1 => {
                            println!("provide length:");
                            input= String::new();
                            std::io::stdin().read_line(&mut input).ok();
                            let input_num: Result<u32, _> = input.trim().parse();

                            let num = match input_num {
                                Ok(num) => num,
                                Err(_) => {
                                    println!("Please input a number!");
                                    continue;
                                }
                            };
                            self.builder.build_car(num,CarType::Passenger);
                        },
                        2 => {
                            println!("provide length:");
                            input= String::new();
                            std::io::stdin().read_line(&mut input).ok();
                            let input_num: Result<u32, _> = input.trim().parse();

                            let num = match input_num {
                                Ok(num) => num,
                                Err(_) => {
                                    println!("Please input a number!");
                                    continue;
                                }
                            };
                            self.builder.build_car(num,CarType::Cargo);
                        },
                        0 => continue,
                        _ => println!("wrong number: {}",num),
                    };
                },
                0 => return,
                _ => println!("wrong number: {}",num),
            };
        }
        
    }
    fn get(&mut self) -> &Train{
        self.builder.get_result()
    }
}

fn main() {
    println!("type 1 to create train with axes, 2 to create train without axes");
    let mut input = String::new();
    std::io::stdin().read_line(&mut input).ok();
    let input_num: Result<u32, _> = input.trim().parse();

    let num = match input_num {
        Ok(num) => num,
        Err(_) => {
            panic!("Please input a number!");
            }
        };
    let axes = match num {
        1 => true,
        2 => false,
        _ => panic!("wrong number: {}",num),
    };


    let mlb = MagLevBuilder::new(axes);
    let mut dir = Director{builder: box mlb};
    dir.construct();
    println!("\n========Constructed=======\n");
    let train = dir.get();
    train.say_hi();
}
