use abstract_factory::train::*;
use super::AbstractTrainFactory;

pub struct AxisTrainFactory;

impl AbstractTrainFactory for AxisTrainFactory {

    fn create_diesel_locomotive(&self, lenght: u32) -> Box<Locomotive>{
        box AxisDieselLocomotive::new(lenght)
    }
    fn create_electric_locomotive(&self, lenght: u32) -> Box<Locomotive>{
        box AxisElectricLocomotive::new(lenght)
    }
    fn create_cargo_car(&self, lenght: u32) -> Box<Car>{
        box AxisCargoCar::new(lenght)
    }
    fn create_passenger_car(&self, lenght: u32) -> Box<Car>{
        box AxisPassengerCar::new(lenght)
    }
}
