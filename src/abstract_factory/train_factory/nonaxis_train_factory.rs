use abstract_factory::train::*;
use super::AbstractTrainFactory;

pub struct NonAxisTrainFactory;

impl AbstractTrainFactory for NonAxisTrainFactory {

    fn create_diesel_locomotive(&self, lenght: u32) -> Box<Locomotive>{
        box NonAxisDieselLocomotive::new(lenght)
    }
    fn create_electric_locomotive(&self, lenght: u32) -> Box<Locomotive>{
        box NonAxisElectricLocomotive::new(lenght)
    }
    fn create_cargo_car(&self, lenght: u32) -> Box<Car>{
        box NonAxisCargoCar::new(lenght)
    }
    fn create_passenger_car(&self, lenght: u32) -> Box<Car>{
        box NonAxisPassengerCar::new(lenght)
    }
}
