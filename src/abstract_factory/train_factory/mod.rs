pub use self::axis_train_factory::AxisTrainFactory;
pub use self::nonaxis_train_factory::NonAxisTrainFactory;

use abstract_factory::train::car::Car;
use abstract_factory::train::locomotive::Locomotive;

pub mod axis_train_factory;
pub mod nonaxis_train_factory;


pub trait AbstractTrainFactory {

    fn create_diesel_locomotive(&self, lenght: u32) -> Box<Locomotive>;
    fn create_electric_locomotive(&self, lenght: u32) -> Box<Locomotive>;
    fn create_cargo_car(&self, lenght: u32) -> Box<Car>;
    fn create_passenger_car(&self, lenght: u32) -> Box<Car>;
}
