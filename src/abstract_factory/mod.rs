pub use self::train::*;
pub use self::train_factory::*;

pub mod train;
pub mod train_factory;

#[test]
fn test(){
    let mut atf: Box<AbstractTrainFactory> = box AxisTrainFactory;
    let mut d_loc:Box<Locomotive> = atf.create_diesel_locomotive(15);
    let mut e_loc:Box<Locomotive> = atf.create_electric_locomotive(10);
    let mut c_car:Box<Car> = atf.create_cargo_car(2);
    let mut p_car:Box<Car> = atf.create_passenger_car(5);
    d_loc.cho_cho();
    e_loc.cho_cho();
    c_car.inspect();
    p_car.inspect();

    atf = box NonAxisTrainFactory;;
    d_loc = atf.create_diesel_locomotive(1);
    e_loc = atf.create_electric_locomotive(44);
    c_car = atf.create_cargo_car(2);
    p_car = atf.create_passenger_car(1);
    d_loc.cho_cho();
    e_loc.cho_cho();
    c_car.inspect();
    p_car.inspect();
}



