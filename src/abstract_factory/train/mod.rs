pub use self::car::*;
pub use self::locomotive::*;

pub mod car;
pub mod locomotive;

pub struct Train {
    pub _locomotives: Vec<Box<Locomotive>>,
    pub _cars: Vec<Box<Car>>,
}

impl Train {
    pub fn new() ->Train {
        Train{_locomotives:vec![],_cars:vec![]}
    }

    pub fn say_hi(&self) {
        for i in self._locomotives.iter(){
            i.cho_cho();
        }
        for i in self._cars.iter(){
            i.inspect();
        }
    }
    pub fn loc_len(&self) -> u32{
        let mut sum: u32 = 0;
        for i in self._locomotives.iter(){
            sum += i.len();
        }
        sum
    }
    pub fn len(&self) -> u32{
        let mut sum: u32 = 0;
        for i in self._locomotives.iter(){
            sum += i.len();
        }
        for i in self._cars.iter(){
            sum += i.len();
        }
        sum
    }
}
