pub use self::diesel_locomotive::AxisDieselLocomotive;
pub use self::diesel_locomotive::NonAxisDieselLocomotive;
pub use self::electric_locomotive::AxisElectricLocomotive;
pub use self::electric_locomotive::NonAxisElectricLocomotive;

pub mod diesel_locomotive;
pub mod electric_locomotive;

pub enum LocomotiveType {
    Diesel,
    Electric,
}

pub trait Locomotive{
    fn cho_cho(&self);
    fn len(&self) -> u32;
}
