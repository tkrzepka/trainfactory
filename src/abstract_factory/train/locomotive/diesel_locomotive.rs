use super::Locomotive;

pub struct AxisDieselLocomotive{
    _length: u32,
    _axes: u32,
}
pub struct NonAxisDieselLocomotive{
    _length: u32,
}

impl AxisDieselLocomotive{
    
    pub fn new(length: u32) -> AxisDieselLocomotive{
        let axes = match length {
            x if x>16 => 7,
            _ => 4
        };
        AxisDieselLocomotive{_length: length, _axes: axes}
    } 
}

impl NonAxisDieselLocomotive{
    
    pub fn new(length: u32) -> NonAxisDieselLocomotive{
        NonAxisDieselLocomotive{_length: length}
    } 
}


impl Locomotive for AxisDieselLocomotive {

    fn cho_cho(&self) {
        println!("Locomotive type: diesel; transport type: axis based; axes: {}; length: {}", self._axes, self._length);
    }
    fn len(&self) -> u32{
        self._length
    }
}

impl Locomotive for NonAxisDieselLocomotive {

    fn cho_cho(&self) {
        println!("Locomotive type: diesel; transport type: maglev; length: {}", self._length);
    }
    fn len(&self) -> u32{
        self._length
    }
}

impl Clone for NonAxisDieselLocomotive {
    fn clone(&self) -> NonAxisDieselLocomotive{
        NonAxisDieselLocomotive{_length: self._length}
    }
}

impl Clone for AxisDieselLocomotive {
    fn clone(&self) -> AxisDieselLocomotive{
        AxisDieselLocomotive{_length: self._length, _axes: self._axes}
    }
}