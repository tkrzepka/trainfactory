use super::Locomotive;

pub struct AxisElectricLocomotive{
    _length: u32,
    _axes: u32,
}
pub struct NonAxisElectricLocomotive{
    _length: u32,
}

impl AxisElectricLocomotive{
    
    pub fn new(length: u32) -> AxisElectricLocomotive{
        let axes = match length {
            x if x>16 => 7,
            _ => 4
        };
        AxisElectricLocomotive{_length: length, _axes: axes}
    } 
}

impl NonAxisElectricLocomotive{
    
    pub fn new(length: u32) -> NonAxisElectricLocomotive{
        NonAxisElectricLocomotive{_length: length}
    } 
}

impl Locomotive for AxisElectricLocomotive {

    fn cho_cho(&self) {
        println!("Locomotive type: electric; transport type: axis based; axes: {}; length: {}", self._axes, self._length);
    }
    fn len(&self) -> u32{
        self._length
    }
}

impl Locomotive for NonAxisElectricLocomotive {

    fn cho_cho(&self) {
        println!("Locomotive type: electric; transport type: maglev; length: {}", self._length);
    }
    fn len(&self) -> u32{
        self._length
    }
}

impl Clone for NonAxisElectricLocomotive {
    fn clone(&self) -> NonAxisElectricLocomotive{
        NonAxisElectricLocomotive{_length: self._length}
    }
}

impl Clone for AxisElectricLocomotive {
    fn clone(&self) -> AxisElectricLocomotive{
        AxisElectricLocomotive{_length: self._length, _axes: self._axes}
    }
}