use super::Car;

pub struct AxisPassengerCar{
    _length: u32,
    _axes: u32,
}
pub struct NonAxisPassengerCar{
    _length: u32,
}

impl AxisPassengerCar{
    
    pub fn new(length: u32) -> AxisPassengerCar{
        let axes = match length {
            x if x>16 => 4,
            _ => 2
        };
        AxisPassengerCar{_length: length, _axes: axes}
    } 
}

impl NonAxisPassengerCar{

    pub fn new(length: u32) -> NonAxisPassengerCar{
        NonAxisPassengerCar{_length: length}
    } 
}

impl Car for AxisPassengerCar {

    fn inspect(&self) {
        println!("Car type: passenger; transport type: axis based; axes: {}; length: {}", self._axes, self._length);
    }
    fn len(&self) -> u32{
        self._length
    }
}

impl Car for NonAxisPassengerCar {

    fn inspect(&self) {
        println!("Car type: passenger; transport type: maglev; length: {}", self._length);
    }
    fn len(&self) -> u32{
        self._length
    }
}

impl Clone for NonAxisPassengerCar {
    fn clone(&self) -> NonAxisPassengerCar{
        NonAxisPassengerCar{_length: self._length}
    }
}

impl Clone for AxisPassengerCar {
    fn clone(&self) -> AxisPassengerCar{
        AxisPassengerCar{_length: self._length, _axes: self._axes}
    }
}