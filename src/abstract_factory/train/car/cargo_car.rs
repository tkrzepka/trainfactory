use super::Car;

pub struct AxisCargoCar{
    _length: u32,
    _axes: u32,
}
pub struct NonAxisCargoCar{
    _length: u32,
}

impl AxisCargoCar{
    
    pub fn new(length: u32) -> AxisCargoCar{
        let axes = match length {
            x if x>16 => 4,
            _ => 2
        };
        AxisCargoCar{_length: length, _axes: axes}
    } 
}

impl NonAxisCargoCar{

    pub fn new(length: u32) -> NonAxisCargoCar{
        NonAxisCargoCar{_length: length}
    } 
}

impl Car for AxisCargoCar {

    fn inspect(&self) {
        println!("Car type: cargo; transport type: axis based; axes: {}; length: {}", self._axes, self._length);
    }
    fn len(&self) -> u32{
        self._length
    }
}

impl Car for NonAxisCargoCar {

    fn inspect(&self) {
        println!("Car type: cargo; transport type: maglev; length: {}", self._length);
    }
    fn len(&self) -> u32{
        self._length
    }
}

impl Clone for NonAxisCargoCar {
    fn clone(&self) -> NonAxisCargoCar{
        NonAxisCargoCar{_length: self._length}
    }
}

impl Clone for AxisCargoCar {
    fn clone(&self) -> AxisCargoCar{
        AxisCargoCar{_length: self._length, _axes: self._axes}
    }
}