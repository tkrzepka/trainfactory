pub use self::cargo_car::AxisCargoCar;
pub use self::cargo_car::NonAxisCargoCar;
pub use self::passenger_car::AxisPassengerCar;
pub use self::passenger_car::NonAxisPassengerCar;

pub mod cargo_car;
pub mod passenger_car;

pub enum CarType {
    Cargo,
    Passenger,
}

pub trait Car{
    fn inspect(&self);
    fn len(&self) -> u32;
}
